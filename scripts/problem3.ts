export{}
const csv = require('csv-parser')
const fs = require('fs')
const results:any = {};
 
fs.createReadStream('../source_data/umpires.csv')
  .pipe(csv())
  .on('data', (data:any) => {
    if(data.Nationality!=='India'){
        if(!results[data.Nationality]){
            results[data.Nationality]=0;
        }
        results[data.Nationality]+=1;
    }
  })
  .on('end', () => {
    fs.writeFile('../frontend/build/json/problem3.json', JSON.stringify(results),()=>{});
  });