export{}
import * as fs from 'fs';
const csv = require('csv-parser');

const results:any = {};
fs.createReadStream('../source_data/deliveries.csv')
  .pipe(csv())
  .on('data', (data:any) => {
      if(!results[data.batting_team]){
          results[data.batting_team]=0;
      }
      results[data.batting_team]+=Number(data.total_runs);
     // console.log(data);
  })
  .on('end', () => {
      fs.writeFile('../frontend/build/json/problem1.json', JSON.stringify(results),()=>{});
  });