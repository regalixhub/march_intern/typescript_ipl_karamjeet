"use strict";
var matches = [];
var seriesArr = [];
fetch('./build/json/problem4.json')
    .then(function (response) {
    response.json().then(function (data) {
        var team = Object.keys(data);
        var series = Object.values(data);
        var years = Object.keys(series[0]);
        for (var i = 0; i < series.length; i++) {
            matches.push(Object.values(series[i]));
        }
        ;
        for (var i = 0; i < team.length; i++) {
            seriesArr.push({
                name: team[i],
                data: matches[i]
            });
        }
        Highcharts.chart('container4', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Stacked column chart'
            },
            xAxis: {
                categories: years
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Number'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: 'gray'
                    }
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'white'
                    }
                }
            },
            series: seriesArr
        });
    });
});
