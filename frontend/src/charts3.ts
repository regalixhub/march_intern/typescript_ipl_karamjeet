fetch('./build/json/problem3.json')
.then(Response => Response.json())
.then((data) => {
      Highcharts.chart('container3', {
         chart : {
           type: 'column',
         },
         title : {
            text: 'Foreign umpire analysis',
         },
         subtitle : {
            text: 'source: ipl.com',  
         },
         xAxis : {
            categories:Object.keys(data),
         },
         yAxis : {
            min: 0,
            title: {
               text: 'umpires by country'         
            }      
         },
         credits : {
         enabled: false
         },
         series: [{
            name:'persons',
            data:Object.values(data)
            }]     
         });
      });
   