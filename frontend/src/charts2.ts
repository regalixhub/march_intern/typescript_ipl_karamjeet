fetch('./build/json/problem2.json')
.then(Response => Response.json())
.then((data) => {
      Highcharts.chart('container2', {
         chart : {
           type: 'column',
         },
         title : {
            text: 'IPL scores of batsmen in Royal Challengers Bangalore',
         },
         subtitle : {
            text: 'source: ipl.com',  
         },
         xAxis : {
            categories:Object.keys(data),
         },
         yAxis : {
            min: 0,
            title: {
               text: 'runs'         
            }      
         },
         credits : {
         enabled: false
         },
         series: [{
            name:'runs',
            data:Object.values(data)
            }]     
         });
      });
   